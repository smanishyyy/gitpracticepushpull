import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Arrays2 {

	public static void main(String arr[]) throws NumberFormatException, IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr2[] = new int[5];

		for (int i = 0; i < 5; i++) {
			System.out.print("Insert data");
			arr2[i] = Integer.parseInt(br.readLine());

		}
		System.out.println("Display array data");
		display(arr2);
		System.out.println("After sorting");
		Arrays.sort(arr2);
		display(arr2);
		System.out.println("Which element to search ? ");
		int i = Integer.parseInt(br.readLine());
		int index = Arrays.binarySearch(arr2, i);

		if (index < 0) {
			System.out.println("Element not found");
		} else {
			System.out.println("Element found at index : " + (index + 1));

		}
		int arr4[] = Arrays.copyOf(arr2, 3);
		display(arr4);

		for (int i2 = 0; i2 < arr4.length; i2++) {
			System.out.println("Fill array");
			arr4[i2] = Integer.parseInt(br.readLine());
			Arrays.fill(arr4, arr4[i2]);
		}

		display(arr4);

	}

	private static void display(int[] arr2) {
		for (int i : arr2) {
			System.out.println("Display : " + i);
		}
	}

}
