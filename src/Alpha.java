import java.util.Arrays;
import java.util.LinkedList;

public class Alpha {

	// Function that checks whether
	// the string is in alphabetical
	// order or not
	static boolean isAlphabaticOrder(String s) {
		// length of the string
		int n = s.length();

		// create a character array
		// of the length of the string
		char c[] = new char[n];
		System.out.println("c " + c[n - 1]);
		// assign the string
		// to character array
		for (int i = 0; i < n; i++) {
			c[i] = s.charAt(i);
		}
		System.out.println("c " + c[n - 1]);
		// sort the character array
		Arrays.sort(c);

		// check if the character array
		// is equal to the string or not
		for (int i = 0; i < n; i++)
			if (c[i] != s.charAt(i))
				return false;

		return true;
	}

	public static void main(String args[]) {
		String s1 = "AaAbaca";
		String s2 = "banish";
		String s3 = "ahanu";
		String s4 = "aankaj";

		LinkedList<String> ll = new LinkedList<>();
		ll.add(s1);
		ll.add(s2);
		ll.add(s3);
		ll.add(s4);

		System.out.println("ll " + ll);
		Object at[] = ll.toArray();
		System.out.println("ll.toString()" + at[1]);
		String syu = " ";
		for (int i = 0; i < ll.size(); i++) {

			System.out.println("nnn " + at[i].toString().charAt(0));
			syu = syu + "" + (at[i].toString()).charAt(0);

		}
		System.out.println("syu " + syu);
		// check whether the string is
		// in alphabetical order or not
		if (isAlphabaticOrder(syu))
			System.out.println("Yes");
		else
			System.out.println("No");

	}
	// This Code is contributed by ANKITRAI1
}